﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Settings
{
    [CreateAssetMenu(fileName = "Unit", menuName = "Settings/Unit")]
    public class UnitData : ScriptableObject
    {
        [SerializeField] private float _speed;
        [SerializeField] private float _speedRotation;
        [Space]
        [SerializeField] private float _speedDash;
        [SerializeField] private float _lenghtDash;
        [Space]
        [SerializeField] private float _forcePush;

        public float Speed => _speed;
        public float SpeedRotation => _speedRotation;

        public float SpeedDash => _speedDash;
        public float LenghtDash => _lenghtDash;
        public float ForcePush => _forcePush;

        private void OnValidate()
        {
            if (_speed < 0)
                _speed = 0;

            if (_speedRotation < 0)
                _speedRotation = 0;

            if (_speedDash < 0)
                _speedDash = 0;

            if (_lenghtDash < 0)
                _lenghtDash = 0;

            if (_forcePush < 0)
                _forcePush = 0;

            if (_speedDash < _speed)
                _speedDash = _speed;
        }
    }
}