﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Settings
{
    [CreateAssetMenu(fileName = "General", menuName = "Settings/General")]
    public class GeneralData : ScriptableObject
    {
        [SerializeField] private uint _numScoreEnemy;
        [SerializeField] private Vector2 _frequencyRandomDashes;


        public uint NumScoreEnemy => _numScoreEnemy;
        public Vector2 FrequencyRandomDashes => _frequencyRandomDashes;

        private void OnValidate()
        {
            if (_frequencyRandomDashes.x < 0)
                _frequencyRandomDashes.x = 0;

            if (_frequencyRandomDashes.y < 0)
                _frequencyRandomDashes.y = 0;

            if (_frequencyRandomDashes.x > _frequencyRandomDashes.y)
                _frequencyRandomDashes.x = _frequencyRandomDashes.y;

        }
    }
}