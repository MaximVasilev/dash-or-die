﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : MonoBehaviour, IInteractivityForAI
{
    public float GetCost()
    {
        return 2f;
    }

    private void OnTriggerEnter(Collider collider)
    {
        var unit = collider.GetComponent<Unit>();
        if (unit != null)
        {
            // action
            Destroy(gameObject);
        }


    }
}
