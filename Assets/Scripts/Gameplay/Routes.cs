﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Routes : MonoBehaviour
{
    [SerializeField] private List<Route> _routes;

    public Route GetRandomRoute()
    {
        return _routes[Random.Range(0, _routes.Count)];
    }    

    public Route GetNearestRoute(Vector3 positionUnit)
    {
        float distanceBest = float.MaxValue;
        Route routeRezult = null;

        foreach (var route in _routes)
        {
            foreach (Transform point in route.Points)
            {
                var distance = Vector3.Distance(positionUnit, point.position);

                if (distanceBest > distance)
                {
                    distanceBest = distance;
                    routeRezult = route;
                }
            }
        }

        return routeRezult;
    }
}
