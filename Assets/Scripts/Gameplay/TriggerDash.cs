﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerDash : MonoBehaviour
{
    private void OnTriggerEnter(Collider collider)
    {
        var unit = collider.GetComponent<UnitMove>();
        
        if (unit != null)
            unit.OnDashed(transform.forward * 100f);
    }
}
