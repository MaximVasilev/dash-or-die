﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lava : MonoBehaviour
{
    [SerializeField] private float _speed;

    private Vector3 _startPostion;
    private void Start()
    {
        _startPostion = transform.position;
        StartCoroutine(ReturnPosition());
    }

    private void Update()
    {
        transform.Translate(-transform.forward * _speed);    
    }

    private IEnumerator ReturnPosition()
    {
        while (true)
        {
            yield return new WaitForSeconds(60f);
            transform.position = _startPostion;
        }
    }
}
