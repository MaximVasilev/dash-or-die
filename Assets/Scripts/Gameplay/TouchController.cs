﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class TouchController : MonoBehaviour, ITouchController
{
    // События на действия игрока.
    public event Action<float, float> Drag;
    public event Action PointerDown;
    public event Action PointerUp;

    private Vector3 _positionDown;
    private Vector3 _positionCurrent;
    private Vector3 _positionRezult;

    public void Init()
    {
        //_phone = gameObject.AddComponent<PhoneManaging>();
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        _positionDown = eventData.position / 200;
        OnDrag(eventData);
        PointerDown?.Invoke();
    }

    public void OnDrag(PointerEventData eventData)
    {
        _positionCurrent = eventData.position / 200;

        _positionRezult = Vector3.ClampMagnitude((_positionCurrent - _positionDown), 1);
       //_positionRezult = _positionRezult.normalized;

        Drag?.Invoke(_positionRezult.x, _positionRezult.y);
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        _positionRezult = Vector2.zero;
        // _phone.VibrateMini(); Выключим для тестов.
        PointerUp?.Invoke();
    }

    public float GetHorizontal()
    {
        return _positionRezult.x;
    }

    public float GetVertical()
    {
        return _positionRezult.y;
    }
}
