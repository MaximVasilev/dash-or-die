﻿internal interface IFixedUpdate
{
    void OnFixedUpdate();
}
