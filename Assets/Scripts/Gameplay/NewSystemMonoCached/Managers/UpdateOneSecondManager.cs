﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class UpdateOneSecondManager : Manager
{
    private List<IUpdateOneSecond> _objs = new List<IUpdateOneSecond>(0);

    private void OnEnable()
    {
        StartCoroutine(UpdateOneSecondDelay());
    }

    private IEnumerator UpdateOneSecondDelay()
    {
        while (enabled == true)
        {
            yield return new WaitForSeconds(1f);
            for (int i = 0; i < _objs.Count; i++)
                _objs[i].OnUpdateOneSecond();
        }
    }
}
