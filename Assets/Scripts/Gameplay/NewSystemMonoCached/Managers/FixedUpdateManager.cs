﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FixedUpdateManager : Manager
{
    private List<IFixedUpdate> _objs = new List<IFixedUpdate>(0);

    private void FixedUpdate()
    {
        for (int i = 0; i < _objs.Count; i++)
            _objs[i].OnFixedUpdate();
    }
}
