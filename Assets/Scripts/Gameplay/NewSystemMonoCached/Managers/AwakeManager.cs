﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AwakeManager : Manager
{
    private List<IAwake> _objs = new List<IAwake>(0);

    private void Awake()
    {
        for (int i = 0; i < _objs.Count; i++)
            _objs[i].OnAwake();

        _objs.Clear();
    }
}
