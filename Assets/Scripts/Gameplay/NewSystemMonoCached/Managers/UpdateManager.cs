﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class UpdateManager : Manager
{
    private List<IUpdate> _objs = new List<IUpdate>(0);

    private void Update()
    {
        for (int i = 0; i < _objs.Count; i++)
            _objs[i].OnUpdate();
    }
}
