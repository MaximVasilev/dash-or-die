﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Unit))]
public class UnitMove : UnitComponent, IMoveable
{
    protected const string TagGround = "Ground";

    [SerializeField] private TriggerDash _triggerDash;

    // Для рассчета передвижения.
    protected float _speed;
    protected float _speedRotation;
    protected float _speedDash;
    protected float _lenghtDash;
    protected float _forcePush;

    protected bool _isGrounded = true;
    private Queue<GameObject> _grounded = new Queue<GameObject>(0);

    protected Vector3 _startPostionDash;

    protected bool _isDash;
    protected bool _isDashed;

    public bool IsDash => _isDash;

    protected bool _isPush;
    protected bool _isPushed;

    public bool IsPushedOrDashed => _isDashed || _isPushed;

    protected Rigidbody _rigidbody;
    private Collider _collider;


    // Сколько раз юнит толкнул одного и того же ии
    protected int _countPushUnit;
    protected Transform _targetPast;

    protected override void Awake()
    {
        base.Awake();
        _rigidbody = GetComponent<Rigidbody>();
        _collider = GetComponent<Collider>();
    }
    private void OnEnable()
    {
        _unit.Dash += OnDash;
        _unit.DisableDash += OffDash;
    }

    private void OnDisable()
    {
        _unit.Dash -= OnDash;
        _unit.DisableDash -= OffDash;
    }

    public virtual void Init(float speed, float speedRotation, float speedDash, float lenghtDash, float forcePush)
    {
        _speed = speed;
        _speedRotation = speedRotation;
        _speedDash = speedDash;
        _lenghtDash = lenghtDash;
        _forcePush = forcePush;
    }
    private void FixedUpdate()
    {
        if (_isPushed == false && _isDashed == false)
        {
            if (_isDash == true)
                Dash();
            else
                Move();
        }
    }

    public void OnPushed(Vector3 force)
    {
        if (_isPushed == false)
            StartCoroutine(Pushed(force));
    }
    private IEnumerator Pushed(Vector3 force)
    {
        PushedOrDashed(force);
        _isPushed = true;
        yield return new WaitForSeconds(_forcePush * 0.5f / 18);
        _isPushed = false;
    }

    public void OnDashed(Vector3 force)
    {
        if (_isDashed == false)
            StartCoroutine(Dashed(force));
    }
    private IEnumerator Dashed(Vector3 force)
    {
        PushedOrDashed(force);
        _isDashed = true;
        yield return new WaitForSeconds(0.5f);
        _isDashed = false;
    }

    private void OnCollisionStay(Collision collision)
    {
        var obj = collision.collider.gameObject;
        var enemyMove = obj.GetComponent<UnitMove>();
        var enemyUnit = obj.GetComponent<Unit>();
        
        if (enemyUnit == null)
            return;

        if (_isDash == true)
        { 
            CollisionDeshes(enemyUnit.GetComponent<Unit>());
        }
        else
        {
            if (IsPushedOrDashed == false)
            {
                if (enemyMove != null)
                {
                    enemyMove.OnPushed(transform.forward * _forcePush * 100f);
                    OnPushed(enemyMove.transform.forward * _forcePush * 100f);
                }
            }
        }

        enemyUnit.Enemy = _unit;
        _unit.Enemy = enemyUnit;   
    }

    private void CollisionDeshes(Unit unit)
    {
        var moveableObj = unit.GetComponent<UnitMove>();
        if (moveableObj != null && moveableObj.IsDash == true)
        {
            unit.OffDash();
            _unit.OffDash();
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        var obj = collision.collider.gameObject;
        if (_isGrounded == true && obj.tag == TagGround)
        {
            _grounded.Enqueue(obj);
            _rigidbody.constraints = RigidbodyConstraints.FreezePositionY | RigidbodyConstraints.FreezeRotation;
        }

        var enemyUnit = obj.GetComponent<Unit>();

        if (enemyUnit != null)
        {
            if (_targetPast == enemyUnit.transform)
                _countPushUnit++;
            else
                _countPushUnit = 0;

            _targetPast = enemyUnit.transform;
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        var obj = collision.collider.gameObject;
        if (obj.tag == TagGround)
        {
            if (_grounded.Count > 0)
                _grounded.Dequeue();

            if (_grounded.Count == 0)
                StartCoroutine(DestroyDelay());
        }
    }

    protected IEnumerator DestroyDelay()
    {
        yield return new WaitForSeconds(0.2f);

        if (_grounded.Count == 0)
        {
            _isGrounded = false;

            _rigidbody.Sleep();
            _rigidbody.WakeUp();
            _rigidbody.AddForce(-transform.up * 500);
            _collider.isTrigger = true;
            enabled = false;
            _rigidbody.constraints = RigidbodyConstraints.FreezeRotation;
        }
    }



    protected virtual void Move()
    {

    }
    protected virtual void Dash()
    {
        _rigidbody.velocity = _speed * transform.forward * _speedDash / 2;
        
        if (Vector3.Distance(_startPostionDash, transform.position) >= _lenghtDash)
            _unit.OffDash();
    }

    protected virtual void PushedOrDashed(Vector3 force)
    {
        _rigidbody.Sleep();
        _rigidbody.WakeUp();
        _rigidbody.constraints = RigidbodyConstraints.FreezePositionY | RigidbodyConstraints.FreezeRotation;
        _rigidbody.AddForce(force);
    }

    public void OnDash()
    {
        _isDash = true;
        _rigidbody.constraints = RigidbodyConstraints.FreezePositionY | RigidbodyConstraints.FreezeRotation;
        _triggerDash.gameObject.SetActive(true);
        _startPostionDash = transform.position;
    }

    public void OffDash()
    {
        _triggerDash.gameObject.SetActive(false);
        _isDash = false;
    }

    public float GetSpeedCurrent()
    {
        return _rigidbody.velocity.magnitude;
    }
}

