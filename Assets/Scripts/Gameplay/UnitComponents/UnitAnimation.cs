﻿using UnityEngine;
using System.Collections;

public class UnitAnimation : UnitComponent
{
    [SerializeField] private Animator _animator;
    private PlayerMove _playerMove;
    private void OnEnable()
    {
        if (_animator == null)
        {
            _animator = GetComponent<Animator>();
            if (_animator == null)
            {
                Debug.LogError("Укажите Animator в UnitAnimation объекта " + name);
                return;
            }
        }

        _playerMove = GetComponent<PlayerMove>();
        _unit.Dash += OnDash;
        _unit.DisableDash += OffDash;
    }

    private void Update()
    {
        if (_playerMove != null)
        {
            if (_playerMove.IsDash == false)
                _animator.speed = _playerMove.GetSpeedCurrentNormal();
            else
                _animator.speed = 1;
        }
    }

    private void OnDisable()
    {
        _unit.Dash -= OnDash;
        //_unit.DisableDash -= OffDash;
        _animator.speed = 0;
    }

    private void OnDash()
    {
        _animator.SetBool("Run", true);
    }
    private void OffDash()
    {
        _animator.SetBool("Run", false);
    }
}
