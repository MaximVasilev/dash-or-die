﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Unit))]
public class AIDash : UnitComponent
{
    [SerializeField] private GameSettings _settings;

    private Rigidbody _rigidbody;
    private UnitPatrol _patrol;
    protected override void Awake()
    {
        base.Awake();
        _rigidbody = GetComponent<Rigidbody>();
        _patrol = GetComponent<UnitPatrol>();

        if (_settings == null)
        {
            Debug.LogError("Добавьте GameSettings в GameSettings объекта " + name);
            return;
        }
    }

    private void OnEnable()
    {
        StartCoroutine(ShotUpdate());
    }

    private void OnDisable()
    {
        StopCoroutine(ShotUpdate());
    }

    private void Update()
    {
        Debug.DrawRay(transform.position + (transform.forward * 4) + (transform.up * transform.localScale.y/2), -transform.up * 3, Color.red);
    }

    /// <summary>
    /// Периодическое наносение ударов ботом, в зависимости от сложности врага.
    /// </summary>
    /// <returns></returns>
    private IEnumerator ShotUpdate()
    {
        while (true)
        {
            var difficulty = Random.Range(_settings.General.FrequencyRandomDashes.x, _settings.General.FrequencyRandomDashes.y);
            yield return new WaitForSeconds(10 / difficulty);
            
            if ((_patrol != null && _patrol.enabled == false) || _patrol == null)
            {
                if (IsZoneDestroy() == false)
                    _unit.OnDash();
            }
        }
    }

    private bool IsZoneDestroy()
    {
        RaycastHit rch;
        if (Physics.Raycast(transform.position + (transform.forward * 4) + transform.up * 2, -transform.up, out rch, 3.00f))
        {
            if (rch.collider.gameObject != null)
            {
                return false;
            }
        }

        return true;
    }
}
