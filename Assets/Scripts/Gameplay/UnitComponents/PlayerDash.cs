﻿using System.Collections;
using UnityEngine;

public class PlayerDash : UnitComponent
{
    [SerializeField] private TouchController _touchController;

    private void OnEnable()
    {
        if(_touchController == null)
        {
            Debug.LogError("Добавьте TouchController в PlayerShot объекта " + name);
            return;
        }    

        _touchController.PointerUp += Dash;
    }

    private void OnDisable()
    {
        _touchController.PointerUp -= Dash;
    }

    private void Dash()
    {
        _unit.OnDash();
    }
}
