﻿using Settings;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

/// <summary>
/// Основной компонент юнита (бота или игрока).
/// </summary>
[RequireComponent(typeof(Rigidbody))]
public class Unit : MonoBehaviour, IInteractivityForAI
{
    // Задержка перед началом ходьбы после удара.
    private const float DelaySecondsToStandUp = 0.4f;

    // События различного поведелия юнита.
    public event Action Dash, DisableDash, Kill, PickUpСoin;
    public new event Action Destroy;

    public UnitScore Score { get; private set; }

    public Unit Enemy { get; set; }

    Transform IInteractivityForAI.transform => transform;

    private void Awake()
    { 
        Score = new UnitScore(0, (uint)PlayerPrefs.GetInt("Record", 0));
    }

    public void Init(uint scoreEnemy)
    {
        Kill += () => AddScoreKill(scoreEnemy);
    }

    private void OnDestroy()
    {
        Destroy?.Invoke();
        if (Enemy != null)
            Enemy.Kill?.Invoke();

        var units = transform.parent.GetComponentsInChildren<Unit>();

        if (Enemy != null && units.Length == 1)
            if (Enemy.GetComponent<WorkWithUI>() != null)
                Enemy.GetComponent<WorkWithUI>().StopGame();
    }

    private void AddScoreKill(uint scoreEnemy)
    {
        Score.AddScore(scoreEnemy);
        Score.AddKill();
    }

    public float GetCost()
    {
        return 1;
    }

    public void OnDash()
    {
        Dash?.Invoke();
    }

    public void OffDash()
    {
        DisableDash?.Invoke();
    }

    public void OnKill()
    {
        Kill?.Invoke();
    }
}

