﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class UnitPatrol : UnitComponent
{
    [SerializeField] private Routes _routes;
    private float ChancePatrolAgain = 50f;

    private NavMeshAgent _agent;
    private Rigidbody _rigidbody;
    private float _distanceToPoint;
    private Transform _target;
    private Route _currentRoute;

    private bool _isForward;

    protected override void Awake()
    {
        base.Awake();
        _agent = GetComponent<NavMeshAgent>();
        _rigidbody = GetComponent<Rigidbody>();
    }

    private void OnEnable()
    {
        _rigidbody.Sleep();
        _rigidbody.WakeUp();
        _agent.enabled = true;
        _currentRoute = _routes.GetRandomRoute();
        _target = _currentRoute.GetNearestPoint(transform.position);
        _agent.destination = _target.position;
        _isForward = false;
        ChancePatrolAgain = 50f;
    }

    private void Update()
    {
        _distanceToPoint = Vector3.Distance(transform.position, _target.position);
        if (_distanceToPoint < 1f)
        {
            if (_currentRoute.IsLastPoint(_target, _isForward) == true)
            {
                _isForward = !_isForward;
                var isDisable = Random.Range(0, 100) <= ChancePatrolAgain ? true : false;
                if (isDisable == true)
                {
                    enabled = false;
                }

                ChancePatrolAgain /= 2;
            }

            _target = _currentRoute.GetNextPoint(_target, _isForward);
            _agent.destination = _target.position;
        }
    }
}
