﻿using UnityEngine;
using System.Collections;
using System;

public class PlayerMove : UnitMove
{
    [SerializeField] private TouchController _touchController;

    private Vector3 _moveVector;
    private void Start()
    {
        if (_touchController == null)
        {
            Debug.LogError("Для успешной работы PlayerMove необходимо указать TouchController объекта " + name);
            return;
        }
    }


    protected override void Dash()
    {
        base.Dash();
    }
    protected override void Move()
    {
        base.Move();

        // Получаем значения тача по горизонтали в вертикали.
        float horizontal = _touchController.GetHorizontal();
        float vertical = _touchController.GetVertical();
        _moveVector = new Vector3(horizontal, 0, vertical);

        // Двигаем объект на вектор заданный ранее со скоростью, указанной в настройках.
        _rigidbody.velocity = _moveVector * _speed;
        Rotation();
    }

    public float GetSpeedCurrentNormal()
    {
        return Mathf.Abs(_touchController.GetHorizontal()) + Mathf.Abs(_touchController.GetVertical());
    }


    /// <summary>
    /// Рассчеты повотора объекта.
    /// </summary>
    private void Rotation()
    {
        // Получаем значения тача по горизонтали в вертикали.
        float horizontal = _touchController.GetHorizontal();
        float vertical = _touchController.GetVertical();

        // Общий вектор передвижения.
        var moveVector = new Vector3(horizontal, 0, vertical);

        // Поворачиваем объект (плавно, с помощью Slerp).
        Vector3 direct = Vector3.RotateTowards(transform.forward, moveVector, _speedRotation, 0);
        transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(direct), Time.fixedDeltaTime * _speedRotation);
    }
}
