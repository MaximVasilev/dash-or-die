﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitAudioEffects : UnitComponent
{
    [SerializeField] private AudioSource _dash;

    private void Start()
    {
        _unit.Dash += DashPlay;
    }

    private void DashPlay()
    {
        _dash.Play();
    }
}
