﻿using System;
using System.Collections;

internal interface IMoveable
{
    void Init(float speed, float speedRotation, float speedDash, float lenghtDash, float forcePush);
}