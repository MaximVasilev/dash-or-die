﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Well : MonoBehaviour
{
    private void OnCollisionStay(Collision collision)
    {
        var unit = collision.collider.gameObject.GetComponent<Unit>();
        if (unit != null)
        {
            if (unit.GetComponent<UnitMove>().IsDash == true)
                unit.OffDash();
        }
    }
}
