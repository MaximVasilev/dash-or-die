﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class Route : MonoBehaviour
{
    [SerializeField] private List<Transform> _points;
    public IEnumerable Points => _points;

    public Transform GetNextPoint(Transform currentPoint, bool isForward)
    {
        var numAdd = isForward == true ? 1 : -1;
        var indexCurrentPoint = _points.IndexOf(currentPoint);
        var rezult = indexCurrentPoint + numAdd;

        return _points[rezult];
    }

    public bool IsLastPoint(Transform currentPoint, bool isForward)
    {
        var indexCurrentPoint = _points.IndexOf(currentPoint);
        var numAdd = isForward == true ? 1 : -1;
        var rezult = indexCurrentPoint + numAdd;

        if (rezult < 0 || rezult > _points.Count - 1)
            return true;

        return false;
    }

    public Transform GetNearestPoint(Vector3 positionUnit)
    {
        float distanceBest = float.MaxValue;
        Transform pointRezult = null;
        
        foreach (Transform point in Points)
        {
            var distance = Vector3.Distance(positionUnit, point.position);

            if (distanceBest > distance)
            {
                distanceBest = distance;
                pointRezult = point;
            }
        }

        return pointRezult;
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;

        if (_points.Count > 0)
        {
            for (int i = 0; i < _points.Count - 1; i++)
                Gizmos.DrawLine(_points[i].position, _points[i + 1].position);

            Gizmos.color = new Color(1, 0, 0, 0.6f);
            for (int i = 0; i < _points.Count; i++)
                Gizmos.DrawWireSphere(_points[i].position, 0.4f);
        }
    }

}
