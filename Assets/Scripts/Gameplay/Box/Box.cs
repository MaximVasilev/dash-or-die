﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Box : MonoBehaviour
{
    private void OnCollisionStay(Collision collision)
    {
        var obj = collision.collider.gameObject;
        if (obj != null)
        {
            var objMoveable = obj.GetComponent<UnitMove>();
            if (objMoveable != null && objMoveable.IsDash == true)
            {
                Destroy(gameObject);
            }
        }
    }
}
