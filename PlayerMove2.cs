﻿using System;
using UnityEngine;

public class PlayerMove : MonoBehaviour
{
    public event Action Stop;

    [SerializeField] private float _speedMax;
    
    public float SpeedMax => _speedMax;
    
    private float _speedCurrent;
    private float _speedRotationCurrent;
    private bool _isGrounded;

    public void Init()
    {

    }

    public bool IsGrounded()
    {
        // _isGrounded = ...
        return _isGrounded;
    }

    #region MonoBehaviour

    // Располагать в порядке выполнения методов
    private void Awake()
    {

    }

    private void OnEnable()
    {

    }

    private void Start()
    {

    }

    private void FixedUpdate()
    {
        if (IsGrounded() == true)
        {
            Move();
            Rotation();
        }
        
    }

    private void OnCollisionEnter(Collision collision)
    {
        var enemy = collision.gameObject.GetComponent<PlayerMove>();

        if (enemy != null)
        {
            OnStop();
        }
    }

    #endregion


    private void Move()
    {
        // _speedCurrent = ...
    }

    private void Rotation()
    {
        // _speedRotationCurrent = ...
    }

    private void OnStop()
    {
        Stop?.Invoke();
    }
}
